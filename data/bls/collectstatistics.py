#!/usr/bin/python3
# Copyright 2013 Bernhard R. Link <brlink@debian.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# SOFTWARE IN THE PUBLIC INTEREST, INC. BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

import datetime, os, re
# use codecs module so this also works with python2
# (once that is no longer needed, do s/codecs.open/open/
import codecs
import subprocess

class RRD:
	def __init__(self, *args):
		self.sp = None
		self.sp = subprocess.Popen(["rrdtool", "-"] + list(args), 0, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
	def close(self):
		if self.sp:
			self.sp.stdin.write("quit\n".encode("UTF-8"))
			self.sp.wait()
			self.sp = None
	def __del__(self):
		self.close()
	def command(self, command):
		self.sp.stdin.write((command + "\n").encode("UTF-8"))
		r = []
		line = ""
		while True:
			line = self.sp.stdout.readline().decode("UTF-8")
			if line is None or line.startswith("OK "):
				break
			if line.startswith("ERROR: "):
				raise Exception("RRD ERROR: %s" % line[7:])
				break
			r.append(line)
		return r


class utc(datetime.tzinfo):
	def utcoffset(self, dt):
		return datetime.timedelta(0)
	def dst(self, dt):
		return datetime.timedelta(0)
def stamp2date(stamp):
	return datetime.datetime.fromtimestamp(int(stamp), utc()).__format__("%Y-%m-%d %H:%M")
def securename(name):
	sn = filter(lambda i: (i >= 'A' and i <= 'Z')or(i >= 'a' and i <= 'z')or(i >= '0' or i <= '9') or i == '-' or i == '_', list(name))
	return "".join(sn)


DATABASE = 'service=qa-buildlogchecks'

DIST = "sid"
dbparms = dict()
dbparms["logs"] = "pub.logs_" + DIST
dbparms["tags"] = "pub.tags_" + DIST
dbparms["stats"] = "pub.stats_" + DIST
dbparms["?"] = "%s"

import psycopg2

conn = psycopg2.connect(DATABASE)

def getall(cmd, params):
	cursor = conn.cursor()
	cursor.execute(cmd.format(**dbparms), params)
	return cursor.fetchall()

tagnumbers = getall("SELECT tag, COUNT(DISTINCT package) FROM {tags} GROUP BY tag ORDER BY tag;", [])
totalcount = getall("SELECT COUNT(DISTINCT package) FROM {logs};", []);
if len(totalcount) <= 0 or len(totalcount[0]) == 0 or totalcount[0][0] <= 0:
	raise Exception("Count not get total package count")
totalcount = totalcount[0][0] * 1.0
rrd = RRD()
for tag, count in tagnumbers:
	#print("%s: %d %f" % (tag, count, 100.0*count/totalcount))
	params = dict()
	params["filename"] = securename(tag) +  ".rrd"
	params["absvalue"] = count
	params["percentvalue"] = 100.0*count/totalcount
	if not os.path.exists(params["filename"]):
		rrd.command("create {filename} --step 86400 DS:absolute:GAUGE:172800:0:1000000 DS:percent:GAUGE:172800:0:100 RRA:MAX:0.5:1:2000 RRA:MIN:0.5:1:400 RRA:AVERAGE:0.5:7:400".format(**params))
	r = rrd.command("update {filename} N:{absvalue}:{percentvalue} ".format(**params))
	if r:
		print(r)
rrd.close()
